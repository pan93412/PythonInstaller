# MiniInstaller
def Install(path1, path2):
    import os
    if os.path.exists(path2) == False:
        os.system("md " + path2)
    else:
        os.system("rd /s /q " + path2)
        os.system("md " + path2)
    os.system("xcopy /E /K /Y /Q " + path1 + " " + path2)
    exit()
def Uninstall(path):
    import os
    if os.path.exists(path) == True:
        os.system("rd /s /q " + path)
        exit()